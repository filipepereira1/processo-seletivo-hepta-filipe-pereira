# Processo-seletivo
Projeto processo seletivo hepta. Filipe Pereira.

Projeto finalizado 06/05/2020 22:00

O projeto foi finalizado conforme pedido, não tive muito tempo de implementar mais funcionalidades, apesar de ser a primeira vez utilizando vuejs, achei bem parecido com Angular. 

# Melhorias
Foi criado um **GenericDAO** para facilitar a persistência de demais entidades. 
Foi criado um **Controller** para receber as requisições via axios e uma classe de serviço para interação com as classes de persistância **DAO**. Foi implementado **DTO** para receber os dados do front-end e enviar os dados para o front-end.
No **front-end** a tela que listava os funcionários foi retirada do index.html e colocado dentro de uma pagina **/funcionarios/pages/listar-funcionarios.html**
As páginas foram estilizadas usando Bootstrap e css e poderão ser encontradas em **pages**.
As páginas **vue** estão dentro de **/vue/**

# Dicas
Na página **index.html** ou **/funcionarios/** foi colocado um botão para acesso a lista de funcionários, dando inicio a tela do sistema.
Na tela de **/funcionarios/pages/novo-funcionario.html** foi implementado também o cadastro de setor e edição do mesmo, para cadastrar um setor é bem simples, basta clicar no botão azul com simbolo de **+** preencher o formulario e salvar.
Para editar basta selecionar um setor e clicar no botao vermelho, fazer alteração e atualizar. 
Na tela **/funcionarios/pages/listar-funcionarios.html**, você poderá excluir um funcionário e editar o mesmo.
Lembrando que não será possivel excluir um setor, apenas alterar o seu nome, e não poderá ter dois setores com o mesmo nome.

# Requisitos

1. Tomcat 9.0
2. JavaSE 1.8

# O que fazer para iniciar o projeto
1. Fazer um git clone https://gitlab.com/filipepereira1/processo-seletivo-hepta-filipe-pereira.git (WINDOWS) ou git@gitlab.com:filipepereira1/processo-seletivo-hepta-filipe-pereira.git (SSH)
2. Adicionar o projeto ao seu Workspace
3. Adicionar o Tomcat 9.0
4. Adicionar o projeto ao Tomcat
5. Criar o banco de dados **funcionarios_prova** (Banco de dados não está sendo criado automaticamente).
6. RUN
7. Acessar o projeto pelo seu navegador favorito localhost:8080/funcionarios

# Ferramentas

1. Spring Tool Suite4
2. Visual Studio Code
3. HeidiSQL
4. Google Chrome



