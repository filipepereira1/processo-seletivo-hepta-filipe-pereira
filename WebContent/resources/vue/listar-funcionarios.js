var inicio = new Vue({
    el: "#inicio",
    data: {
        listaFuncionarios: [],
        quantidadeFuncionariosRegistrados: '',
        funcionarioPesquisado: '',
        listaFuncionariosOriginal: '',
        urlFuncionarios: "/funcionarios/rs/funcionarios/",

    },
    created: function () {

        this.quantidadeFuncionariosCadastrados();

        let vm = this;
        vm.buscaFuncionarios();

    },
    methods: {
        buscaFuncionarios: function () {
            const vm = this;
            axios.get("/funcionarios/rs/funcionarios")
                .then(response => {
                    vm.listaFuncionarios = response.data;
                }).catch(function (error) {
                    alert("Erro interno Não foi possível listar os funcionários");
                }).finally(function () { });
        },

        editarFuncionario: function (funcionario) {
            window.location = "/funcionarios/pages/novo-funcionario.html?id=" + funcionario.id;
        },
        excluirFuncionario(funcionario) {
            axios.delete("/funcionarios/rs/funcionarios/" + funcionario.id).then(response => {
                alert("Funcionário Excluído!");
            }).catch(function (error) {
                vm.mostraAlertaErro("Erro interno", "Não Foi possível deletar o Funcionário");
            }).finally(function () {
                window.location.reload();
            });;

        },
        formatSalario(value) {
            let val = (value / 1).toFixed(2).replace('.', ',')
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        },


        /* Pesquisa Filtrada */
        pesquisarFuncionarios() {
            let pesquisa = this.funcionarioPesquisado.toUpperCase();

            if (this.listaFuncionariosOriginal === '') {
                this.listaFuncionariosOriginal = this.listaFuncionarios;
            }

            if (this.listaFuncionariosOriginal != '') {
                this.listaFuncionarios = this.listaFuncionariosOriginal;
            }

            var funcionariosTEMP = [];

            this.listaFuncionarios.forEach(funcionario => {
                this.listaFuncionarios = funcionariosTEMP;

                if (!funcionario.nome.toLowerCase().indexOf(pesquisa.toLowerCase())) {
                    funcionariosTEMP.push(funcionario);
                }

                this.listaFuncionarios = funcionariosTEMP;
            });

        },
        quantidadeFuncionariosCadastrados() {
            axios.get(this.urlFuncionarios + "registros").then(response => {
                this.quantidadeFuncionariosRegistrados = response.data;
            });
        }
    }
});