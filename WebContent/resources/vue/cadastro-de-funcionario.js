var app = new Vue({

    el: '#app',
    data() {
        return {
            funcionario: {
                id: '',
                nome: '',
                email: '',
                idade: '',
                salario: '',
                setor: {
                    id: '',
                    nome: ''
                }
            },
            setor: {
                id: '',
                nome: ''
            },
            listaSetores: [],
            setorSelecionado: '',
            idFuncionario: '',
            atualizar: false,
            myModel: false,
            errors: [],
            setorAtualizado: false

        }
    },
    created() {

        const func = this;
        func.buscarSetores();

        const urlParams = new URLSearchParams(window.location.search);
        const idFuncionario = urlParams.get('id');

        if (idFuncionario != null) {
            this.funcionario.id = idFuncionario;
            this.atualizar = true;
            this.buscarFuncionarioById();
        }

    },
    methods: {
        salvarFuncionario() {
            const vm = this;
            this.funcionario.setor = this.setorSelecionado;

            axios.post("/funcionarios/rs/funcionarios", this.funcionario)
                .then(response => {
                    alert('Funcionario Salvo com Sucesso!')
                    this.limparFomrmulario();

                    window.location = "/funcionarios/pages/listar-funcionarios.html";

                }).catch(function (error) {
                    alert("Erro interno", "Não foi possivel adicionar o funcionario");
                }).finally(function () { });


        },
        buscarFuncionarioById() {

            axios.get("/funcionarios/rs/funcionarios/" + this.funcionario.id).then(response => {
                const func = response.data;

                this.funcionario.id = func.id;
                this.funcionario.nome = func.nome;
                this.funcionario.email = func.email;
                this.funcionario.salario = func.salario;
                this.funcionario.idade = func.idade;
                this.setorSelecionado = func.setor;

            }).catch(function (error) {
                alert("Erro interno", "Não foi possível buscar o Funcionário");
            }).finally(function () { });


        },
        atualizarFuncionario() {
            this.funcionario.setor = this.setorSelecionado;

            var check = this.checkForm();

            if (check === true) {
                axios.put("/funcionarios/rs/funcionarios/" + this.funcionario.id, this.funcionario).then(response => {
                    response
                    alert("Funcionário Atualizado");
                    window.location = "/funcionarios/pages/listar-funcionarios.html";
                });
            }
        },


        buscarSetores() {
            const vm = this;
            axios.get("/funcionarios/rs/setores")
                .then(response => {
                    vm.listaSetores = response.data;
                }).catch(function (error) {
                    alert("Erro interno", "Não foi buscar os setores");
                }).finally(function () { });
        },

        limparFomrmulario() {

            this.funcionario.nome = '';
            this.funcionario.id = '';
            this.funcionario.email = '';
            this.funcionario.idade = '';
            this.funcionario.salario = '';
            this.funcionario.setor.nome = '';
            this.funcionario.setor.id = '';
        },
        openModelSalvarSetor() {
            this.setor.id = '';
            this.setor.nome = '';
            this.myModel = true;
        },
        salvarSetor() {

            axios.post("/funcionarios/rs/setores", this.setor).then(response => {
                const setorCadastrado = response.data;
                this.myModel = false;
                this.listaSetores.push(setorCadastrado);
                this.funcionario.setor = this.setor;
                this.setorSelecionado = setorCadastrado

                alert("Setor Cadastrado!")
            }).catch(function (error) {

                console.log(error);
                alert("Erro interno, Não foi possível salvar, verifique se o setor já está cadastrado");
            }).finally(function () { });;
        },

        openModelAtualizarSetor() {
            this.setorAtualizado = true;
            this.setor.id = this.setorSelecionado.id;
            this.setor.nome = this.setorSelecionado.nome;
            this.myModel = true;
        },
        atualizarSetor() {

            const vm = this;
            axios.put("/funcionarios/rs/setores/" + this.setor.id, this.setor).then(response => {

                const setorEditado = response.data;

                this.setorSelecionado = setorEditado;
                this.funcionario.setor = this.setor;
                this.listaSetores.push(setorEditado);
                this.myModel = false;
                this.buscarSetores();

                alert("Setor Atualizado");

            }).catch(function (error) {
                alert("Erro interno", "Não foi possível atualizar o setor");
            }).finally(function () {

            });

        },
        checkForm() {
            if (this.funcionario.nome && this.funcionario.email && this.funcionario.idade && this.funcionario.salario && this.funcionario.idade && this.setorSelecionado) {
                this.errors = [];
                if (!this.funcionario.id) {
                    this.salvarFuncionario();
                }
                return true;
            }
            this.errors = [];
            if (!this.funcionario.nome) {
                this.errors.push("Nome é obrigatório!");
            }

            if (!this.funcionario.email) {
                this.errors.push("E-mail é obrigatório!");
            }

            if (!this.funcionario.salario) {
                this.errors.push("Salário é obrigatório!");
            }
            if (!this.funcionario.idade) {
                this.errors.push("Idade é obrigatório!");
            }
            if (!this.setorSelecionado) {
                this.errors.push("Setor é obrigatório!");
            }

            if (this.funcionario.nome.length > 50) {
                this.errors.push("Nome deve ter um tamanho Máximo de 50 Caracteres");
            }

            if (this.funcionario.email.length > 50) {
                this.errors.push("E-mail deve ter um tamanho Máximo de 50 Caracteres");
            }

            if (this.funcionario.salario > 10000) {
                this.errors.push("Salário deve ter um valor Máximo de 10.000");
            }

            if (this.funcionario.idade > 95) {
                this.errors.push("Idade deve ser menor que 95 ");
            }

            if (this.funcionario.idade < 18) {
                this.errors.push("Idade deve ser maior que 18 anos")
            }
        },
        fecharModel() {
            this.myModel = false;
            this.setorAtualizado = false;
        }
    }
})