package com.hepta.funcionarios.rest;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.entity.dto.FuncionarioDTO;
import com.hepta.funcionarios.service.FuncionarioService;

class FuncionarioServiceTest {

	FuncionarioController funcionarioController = new FuncionarioController();

	FuncionarioService funcionario = new FuncionarioService();

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@Test
	void testFuncionarioRead() throws Exception {
		funcionarioController.funcionarioRead();

	}

	@Test
	void testFuncionarioCreate() {

		FuncionarioDTO funcionarioDTO = new FuncionarioDTO();

		funcionarioDTO.setNome("Jo�o Pedro23333");
		funcionarioDTO.setEmail("jo�opedro3233@yahoo.com");
		funcionarioDTO.setSalario(new BigDecimal(1500));
		funcionarioDTO.setIdade(222);
		/*
		 * SetorDTO setorDTO = new SetorDTO();
		 * 
		 * setorDTO.setId(1L); setorDTO.setNome("PADEIRO");
		 * 
		 * funcionarioDTO.setSetor(setorDTO);
		 */
		funcionarioController.funcionarioCreate(funcionarioDTO);

		// fail("Not yet implemented");
	}

	@Test
	void testFuncionarioUpdate() {
		FuncionarioDTO funcionarioDTO = new FuncionarioDTO();

		funcionarioDTO.setId(2L);
		funcionarioDTO.setEmail("update@gmail.com");
		funcionarioDTO.setIdade(22);
		funcionarioDTO.setSalario(new BigDecimal(1500));
		funcionarioDTO.setNome("update update");

		Setor setor = new Setor();
		setor.setId(3L);

		funcionarioDTO.setSetor(setor);

		funcionarioController.funcionarioUpdate(1L, funcionarioDTO);

	}

	@Test
	void testFuncionarioDelete() {

		funcionarioController.funcionarioDelete(11L);
	}

	@Test
	void getTotalRegistros() throws Exception {
		Long quantidade = funcionario.getTotalRegistrosFuncionarios();

		for (int i = 1; i <= quantidade; i++) {
			if (i <= quantidade) {
				System.out.println(1 + i++);
			}
		}
	}

}
