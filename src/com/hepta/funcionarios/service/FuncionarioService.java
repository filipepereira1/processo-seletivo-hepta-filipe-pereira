package com.hepta.funcionarios.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.entity.dto.FuncionarioDTO;
import com.hepta.funcionarios.persistence.GenericDAO;

public class FuncionarioService implements Serializable {

	private static final long serialVersionUID = 1L;

	private GenericDAO<Funcionario> daoFuncionario = null;
	private SetorService setorService = null;

	public FuncionarioService() {
		this.daoFuncionario = new GenericDAO<Funcionario>();
		this.setorService = new SetorService();

	}

	public Funcionario createFuncionario(FuncionarioDTO funcionarioDTO) throws Exception {

		Funcionario funcionario = new Funcionario(funcionarioDTO);

		if (funcionario.getSetor().getId() != null) {
			funcionario.setSetor(setorService.buscarSetorPorId(funcionario.getSetor().getId(), funcionario.getSetor()));
		}

		return daoFuncionario.save(funcionario);

	}

	public List<FuncionarioDTO> findAllFuncionarios() throws Exception {

		List<Funcionario> listaFuncionario = daoFuncionario.getAll(Funcionario.class);
		return mountListFuncionarioDTO(listaFuncionario);

	}

	private List<FuncionarioDTO> mountListFuncionarioDTO(List<Funcionario> listaFuncionario) {

		List<FuncionarioDTO> listaFuncionarioDTO = new ArrayList<FuncionarioDTO>();

		for (Funcionario funcionario : listaFuncionario) {
			FuncionarioDTO funcionarioDTO = new FuncionarioDTO(funcionario);

			listaFuncionarioDTO.add(funcionarioDTO);

		}

		return listaFuncionarioDTO;
	}

	public void deletarFuncionario(Long id) throws Exception {

		Funcionario funcionarioBuscado = daoFuncionario.find(id, new Funcionario());
		if (funcionarioBuscado == null) {

		}
		daoFuncionario.delete(id, new Funcionario());

	}

	public FuncionarioDTO updateFuncionario(Long id, FuncionarioDTO funcionarioDTO) throws Exception {

		Funcionario funcionarioBuscado = daoFuncionario.find(id, new Funcionario(funcionarioDTO));

		funcionarioBuscado.setNome(funcionarioDTO.getNome());
		funcionarioBuscado.setSetor(funcionarioDTO.getSetor());
		funcionarioBuscado.setSalario(funcionarioDTO.getSalario());
		funcionarioBuscado.setEmail(funcionarioDTO.getEmail());
		funcionarioBuscado.setIdade(funcionarioDTO.getIdade());

		Setor setor = setorService.buscarSetorPorId(funcionarioDTO.getSetor().getId(), funcionarioDTO.getSetor());
		funcionarioBuscado.setSetor(setor);

		Funcionario funcionarioAtualizado = daoFuncionario.update(funcionarioBuscado);

		return new FuncionarioDTO(funcionarioAtualizado);

	}

	public FuncionarioDTO findFuncionarioById(Long id) throws Exception {

		return new FuncionarioDTO(daoFuncionario.find(id, new Funcionario()));

	}

	public Long getTotalRegistrosFuncionarios() throws Exception {

		return daoFuncionario.getTotalRegistros(Funcionario.class);
	}

}
