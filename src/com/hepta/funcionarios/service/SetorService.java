package com.hepta.funcionarios.service;

import java.util.List;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.entity.dto.SetorDTO;
import com.hepta.funcionarios.persistence.GenericDAO;

public class SetorService {

	private GenericDAO<Setor> daoSetor = new GenericDAO<Setor>();

	public Setor buscarSetorPorId(Long id, Setor setor) throws Exception {

		if (setor.getId() != null) {

			Setor setorBuscado = daoSetor.find(id, setor);
			return setorBuscado;
		}

		return saveSetor(new SetorDTO(setor));

	}

	public Setor saveSetor(SetorDTO setorDTO) throws Exception {

		Setor setor = new Setor(setorDTO);
		return daoSetor.save(setor);

	}

	public List<Setor> buscarTodosSetores() throws Exception {
		return daoSetor.getAll(Setor.class);
	}

	public void removeSetor(Long id) throws Exception {

		daoSetor.delete(id, new Setor());

	}

	public SetorDTO updateSetor(Long id, SetorDTO setorDTO) throws Exception {

		return (new SetorDTO(daoSetor.update(new Setor(setorDTO))));

	}
}
