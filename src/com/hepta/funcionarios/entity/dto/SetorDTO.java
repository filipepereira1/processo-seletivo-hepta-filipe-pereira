package com.hepta.funcionarios.entity.dto;

import com.hepta.funcionarios.entity.Setor;

public class SetorDTO {

	public SetorDTO(Setor setor) {
		this.id = setor.getId();
		this.nome = setor.getNome();
	}

	public SetorDTO() {

	}

	private Long id;
	private String nome;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome.toUpperCase();
	}

}
