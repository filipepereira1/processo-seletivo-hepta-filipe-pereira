package com.hepta.funcionarios.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.hepta.funcionarios.entity.dto.SetorDTO;

@Entity
public class Setor implements Serializable {

	private static final long serialVersionUID = 1L;

	public Setor(SetorDTO setorDTO) {
		this.id = setorDTO.getId();
		this.nome = setorDTO.getNome();
	}

	public Setor() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_SETOR")
	private Long id;

	@Column(name = "NOME", unique = true)
	private String nome;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
