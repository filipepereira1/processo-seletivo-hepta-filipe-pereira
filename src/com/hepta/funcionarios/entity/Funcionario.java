package com.hepta.funcionarios.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.hepta.funcionarios.entity.dto.FuncionarioDTO;

@Entity
public class Funcionario implements Serializable {

	private static final long serialVersionUID = 1L;

	public Funcionario(FuncionarioDTO funcionarioDTO) {
		this.id = funcionarioDTO.getId();
		this.nome = funcionarioDTO.getNome();
		this.email = funcionarioDTO.getEmail();
		this.salario = funcionarioDTO.getSalario();
		this.idade = funcionarioDTO.getIdade();
		this.setor = funcionarioDTO.getSetor();
	}

	public Funcionario() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_FUNCIONARIO")
	private Long id;

	@Column(name = "NOME")
	private String nome;

	@ManyToOne
	@JoinColumn(name = "FK_SETOR")
	private Setor setor;

	@Column(name = "NU_SALARIO")
	private BigDecimal salario;

	@Column(name = "DS_EMAIL")
	private String email;

	@Column(name = "NU_IDADE")
	private Integer idade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome.toUpperCase();
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public BigDecimal getSalario() {
		return salario;
	}

	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	@Override
	public String toString() {
		return "Funcionario [id=" + id + ", nome=" + nome + ", setor=" + setor + ", salario=" + salario + ", email="
				+ email + ", idade=" + idade + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
