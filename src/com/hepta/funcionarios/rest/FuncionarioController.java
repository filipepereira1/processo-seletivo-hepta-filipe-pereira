package com.hepta.funcionarios.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.dto.FuncionarioDTO;
import com.hepta.funcionarios.service.FuncionarioService;

@Path("/funcionarios")
public class FuncionarioController {

	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	private FuncionarioService funcionarioService = null;

	public FuncionarioController() {
		funcionarioService = new FuncionarioService();
	}

	protected void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	/**
	 * Adiciona novo Funcionario
	 * 
	 * @param Funcionario: Novo Funcionario
	 * @return response 201 (CREATED) - Conseguiu adicionar
	 */
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	public Response funcionarioCreate(FuncionarioDTO funcionarioDTO) {

		try {
			return Response.status(Status.CREATED).entity(funcionarioService.createFuncionario(funcionarioDTO)).build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao salvar o Funcionario").build();
		}

	}

	/**
	 * Lista todos os Funcionarios
	 * 
	 * @return response 200 (OK) - Conseguiu listar
	 * @throws Exception
	 */
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response funcionarioRead() throws Exception {
		try {
			return Response.status(Status.OK).entity(funcionarioService.findAllFuncionarios()).build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar Funcionarios").build();
		}
	}

	/**
	 * Atualiza um Funcionario
	 * 
	 * @param id:          id do Funcionario
	 * @param Funcionario: Funcionario atualizado
	 * @return response 200 (OK) - Conseguiu atualizar
	 */
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@PUT
	public Response funcionarioUpdate(@PathParam("id") Long id, FuncionarioDTO funcionarioDTO) {

		try {
			funcionarioService.updateFuncionario(id, funcionarioDTO);
			return Response.status(Status.OK).build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao fazer update do Funcionario").build();
		}

	}

	/**
	 * Remove um Funcionario
	 * 
	 * @param id: id do Funcionario
	 * @return response 200 (OK) - Conseguiu remover
	 */
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@DELETE
	public Response funcionarioDelete(@PathParam("id") Long id) {

		try {
			funcionarioService.deletarFuncionario(id);
			return Response.status(Status.NO_CONTENT).entity("Funcionario Exclu�do").build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao deletar Funcionario").build();
		}
	}

	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response findFuncionarioById(@PathParam("id") Long id) {

		try {
			return Response.status(Status.OK).entity(funcionarioService.findFuncionarioById(id)).build();
		} catch (Exception e) {
			return Response.status(Status.NO_CONTENT).entity("Funcion�rio n�o encontrado").build();
		}

	}

	@Path("/registros")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response findQuantidadeFuncionarios() {

		try {
			return Response.status(Status.OK).entity(funcionarioService.getTotalRegistrosFuncionarios()).build();
		} catch (Exception e) {
			return Response.status(Status.NO_CONTENT).entity("Funcion�rio n�o encontrado").build();
		}

	}

}
