package com.hepta.funcionarios.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.dto.SetorDTO;
import com.hepta.funcionarios.service.SetorService;

@Path("/setores")
public class SetorController {

	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	private SetorService setorService = null;

	public SetorController() {
		setorService = new SetorService();
	}

	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response setorRead() throws Exception {
		try {
			return Response.status(Status.OK).entity(setorService.buscarTodosSetores()).build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar Setores").build();
		}

	}

	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	public Response createSetor(SetorDTO setorDTO) {
		try {
			return Response.status(Status.CREATED).entity(setorService.saveSetor(setorDTO)).build();
		} catch (Exception e) {
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@DELETE
	public Response deleteSetor(@PathParam("id") Long id) {
		try {

			setorService.removeSetor(id);

		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao deletar Setor").build();
		}
		return Response.status(Status.NO_CONTENT).build();

	}

	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@PUT
	public Response setorUpdate(@PathParam("id") Long id, SetorDTO setorDTO) {

		try {
			return Response.status(Status.OK).entity(setorService.updateSetor(id, setorDTO)).build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao fazer update do Funcionario").build();
		}

	}

}
