package com.hepta.funcionarios.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class GenericDAO<T> {

	public T save(T t) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(t);
			em.getTransaction().commit();

			return t;
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
	}

	public T update(T t) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		T objetoAtualizado = null;
		try {
			em.getTransaction().begin();
			objetoAtualizado = em.merge(t);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return objetoAtualizado;
	}

	@SuppressWarnings("unchecked")
	public void delete(Long id, T entidade) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();

		T t = null;
		try {
			em.getTransaction().begin();
			t = (T) em.find(entidade.getClass(), id);
			em.remove(t);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}

	}

	@SuppressWarnings("unchecked")
	public T find(Long id, T entidade) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();

		T t = null;
		try {

			t = (T) (em.find(entidade.getClass(), id));
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return t;
	}

	@SuppressWarnings("unchecked")
	public List<T> getAll(Class<T> entidade) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		List<T> t = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM " + entidade.getName());

			t = query.getResultList();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return t;
	}

	@SuppressWarnings("unchecked")
	public List<T> getAllSize(Integer size, Class<T> entidade) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		List<T> t = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM " + entidade.getName()).setFirstResult(0).setMaxResults(size);

			t = query.getResultList();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return t;
	}

	public Long getTotalRegistros(Class<T> entidade) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();

		Long t = null;
		try {
			Query query = em.createQuery("select count(*) From " + entidade.getName());

			t = (Long) query.getSingleResult();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return t;
	}

}
